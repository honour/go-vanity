import { IHTTPMethods, Router } from "itty-router";
import { json, redirect } from "./utils";
import { fallback, github, gitlab } from "./packages";

const withPath = (request: Request & { path: string, response: Response }) => {
	request.path = new URL(request.url).pathname;
};

const router = Router<Request, IHTTPMethods>();

router.get("/", async () => {
	const packages = await PACKAGES.list();
	const body = [];

	for (const p of packages.keys)
		body.push(p.name);

	return json({ success: true, packages: body }, {
		headers: {
			"access-control-allow-origin": "*",
			"access-control-allow-methods": "GET",
			"access-control-allow-headers": "*"
		}
	});
});

router.get("*", withPath, async ({ path, url }) => {
	const root = path.substring(1);

	if (!root)
		return redirect(DOMAIN);

	const p = await PACKAGES.get(root, { type: "text" });

	if (!p)
		return json({ success: false, message: "Package not found" }, { status: 404 });

	const { searchParams, host, pathname } = new URL(url);
	const redirectUrl = `https://pkg.go.dev/${host}${pathname}`;

	if (searchParams.get("go-get") !== "1")
		return redirect(redirectUrl);

	const packageUrl = new URL(p);
	const packageHost = packageUrl.host;
	const pathParts = packageUrl.pathname.split("/");

	const user = pathParts[1];
	const repository = pathParts[2];
	const importPath = host + "/" + root;

	switch (packageHost) {
		case "github.com":
			return github(importPath, user, repository, redirectUrl);
		case "gitlab.com":
			return gitlab(importPath, user, repository, redirectUrl);
		default:
			return fallback(importPath, packageHost, user, repository, redirectUrl);
	}
});

router.all("*", () => redirect(DOMAIN));

addEventListener("fetch", event =>
	event.respondWith(router
		.handle(event.request)
		.catch(error => {
			console.error(error);

			return json({ success: false, message: "An unknown error occurred" }, { status: 500 });
		})
	)
);
