import { html } from "./utils";

const template = `<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
{{TAGS}}
<meta http-equiv="refresh" content="0; url={{REDIRECT}}">
</head>
<body>
You should go <a href="{{REDIRECT}}">here</a> instead
</body>
</html>`;

export const handler = (tags: string[], redirectUrl: string): Response => {
	const body = template
		.replaceAll("{{TAGS}}", tags.join("\n"))
		.replaceAll("{{REDIRECT}}", redirectUrl);

	return html(body, {
		headers: {
			"cache-control": "public, max-age=300"
		}
	});
};

export const github = (importPath: string, user: string, repository: string, redirectUrl: string): Response => {
	const repoImportPath = "github.com/" + user + "/" + repository;
	const httpRepoPath = `https://${repoImportPath}`;
	const tags = [
		`<meta name="go-import" content="${importPath} git ${httpRepoPath}">`,
		`<meta name="go-source" content="${importPath} ${httpRepoPath} ${httpRepoPath}/tree/master{/dir} ${httpRepoPath}/blob/master{/dir}/{file}#L{line}">`
	];

	return handler(tags, redirectUrl);
};

export const gitlab = (importPath: string, user: string, repository: string, redirectUrl: string): Response => {
	const repoImportPath = "gitlab.com/" + user + "/" + repository;
	const httpRepoPath = `https://${repoImportPath}`;
	const tags = [
		`<meta name="go-import" content="${importPath} git ${httpRepoPath}">`,
		`<meta name="go-source" content="${importPath} ${httpRepoPath} ${httpRepoPath}/-/tree/master{/dir} ${httpRepoPath}/-/blob/master{/dir}/{file}#L{line}">`
	];

	return handler(tags, redirectUrl);
};

export const fallback = (importPath: string, host: string, user: string, repository: string, redirectUrl: string): Response => {
	const repoImportPath = host + "/" + user + "/" + repository;
	const httpRepoPath = `https://${repoImportPath}`;
	const tags = [
		`<meta name="go-import" content="${importPath} git ${httpRepoPath}">`
	];

	return handler(tags, redirectUrl);
};
