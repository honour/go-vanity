export const json = (body: object, options: ResponseInit = {}): Response => {
	const { headers = {}, ...rest } = options;

	return new Response(JSON.stringify(body), {
		headers: {
			"Content-Type": "application/json",
			...headers
		},
		...rest
	});
};

export const html = (body: string, options: ResponseInit = {}): Response => {
	const { headers = {}, ...rest } = options;

	return new Response(body, {
		headers: {
			"Content-Type": "text/html",
			...headers
		},
		...rest
	});
};

export const redirect = (location: string): Response => {
	return new Response(null, {
		status: 302,
		headers: {
			location: location
		}
	});
};
